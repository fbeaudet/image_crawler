import StringIO
import csv

def convert_to_csv(array):
    line = StringIO.StringIO()
    writer = csv.writer(line)
    writer.writerow(array)
    return line.getvalue()