#!/usr/bin/env python

from flask import Flask, request
import pika
import json

from lib.database import create_database_connection
from config import config

app = Flask(__name__)

@app.route('/', methods=['POST'])
def request_crawling_jobs():
    url = request.args.get('url')
    threads = request.args.get('threads')
    urls = (map(str.strip, request.get_data().split(',')))
    request_id = create_crawling_jobs(urls, threads if threads != None else "2")
    return str(request_id)

@app.route('/status', methods=['GET'])
def get_crawling_jobs_request_results():
    _id = request.args.get('id')
    if _id == None:
        return "Missing non-optional parameter: id"

    database_connection = create_database_connection()
    cursor = database_connection.cursor()
    jobs = cursor.execute("SELECT url, results FROM crawling_jobs WHERE request_id = " + _id).fetchall()
    jobs_done = not (True in map(lambda j: not j[1], jobs))

    results = []
    for x in range(len(jobs)):
        if not jobs_done:
            results.append({ "url" : jobs[x][0], "completed" : True if jobs[x][1] else False })
        else:
            results.append({ "url" : jobs[x][0], "results" : jobs[x][1].split(',') })

    return json.dumps(results)

def create_crawling_jobs(urls, threads):
    database_connection = create_database_connection()
    cursor = database_connection.cursor()
    cursor.execute("INSERT INTO crawling_requests DEFAULT VALUES")
    request_id = cursor.lastrowid
    database_connection.commit()
    job_ids = []
    for x in range(len(urls)):
        cursor.execute("INSERT INTO crawling_jobs (url, threads, request_id) VALUES (\"" + urls[x] + "\"," + threads + ", " + str(request_id) + ")")
        job_ids.append(cursor.lastrowid)
        database_connection.commit()
    push_jobs_to_queue(job_ids)
    database_connection.close()
    return request_id

def push_jobs_to_queue(job_ids):
    queue_connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
    channel = queue_connection.channel()
    channel.queue_declare(queue=config["url_crawling_jobs_queue"])
    for x in range(len(job_ids)):
        channel.basic_publish(exchange='', routing_key=config["url_crawling_jobs_queue"], body=str(job_ids[x]))
    queue_connection.close()

app.run(host='0.0.0.0', port=8080)