from lib.parser import Parser
import pika
import time

from lib.database import create_database_connection
from lib.string_helpers import convert_to_csv
from config import config

class Crawler:

    def __init__(self, job_id, threads = 2, channel_key = "default_crawling_channel", starting_url = "http://example.com", max_depth = 2):
        self.channel_key = channel_key
        self.job_id = job_id
        self.init_time = time.time()
        self.extracted_image_urls = set()
        self.max_depth = max_depth
        self.parsers = map(lambda t: Parser(self, channel_key, t), range(threads))
        self.threads = threads
        self.urls_to_visit = set()
        self.visited_urls = set()
        self.push_url_to_process(starting_url)

    def save_results_if_job_is_done(self):
        if len(self.visited_urls) == 0 or len(self.visited_urls) != len(self.urls_to_visit):
            return False
        now = time.time()
        database_connection = create_database_connection()
        results = convert_to_csv(list(self.extracted_image_urls))[0:-2] #removes \r\n
        cursor = database_connection.cursor()
        cursor.execute("UPDATE crawling_jobs SET results = '" + results + "' WHERE id = " + str(self.job_id))
        database_connection.commit()
        database_connection.close()
        
    def push_url_to_process(self, url, depth = 0):
        if url in self.urls_to_visit or url[0:1] == "#" or url[0:10] == "javascript":
            return
        if url[0:2] == "//" : url = "https:" + url
        self.urls_to_visit.add(url)
        self.push_url_to_queue(url, depth)

    def push_url_to_queue(self, url, depth):
        queue_connection = pika.BlockingConnection(pika.ConnectionParameters(config["queue_connection_string"]))
        channel = queue_connection.channel()
        queue = channel.queue_declare(queue=self.channel_key)
        csv_body = convert_to_csv([url, depth])
        channel.basic_publish(exchange='', routing_key=self.channel_key, body=csv_body)
        queue_connection.close()

    def push_visited_url(self, url):
        self.visited_urls.add(url)

    def update_extracted_image_urls(self, urls):
        self.extracted_image_urls = self.extracted_image_urls.union(urls)
