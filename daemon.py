from lib.parser import Parser
from lib.crawler import Crawler
import pika
import time

from lib.database import create_database_connection
from config import config

queue_connection = pika.BlockingConnection(pika.ConnectionParameters(config["queue_connection_string"]))
channel = queue_connection.channel()
channel.queue_declare(queue=config["url_crawling_jobs_queue"])

def start_crawling(ch, method, properties, body):
    database_connection = create_database_connection()
    cursor = database_connection.cursor()
    id = body
    job = cursor.execute("SELECT * FROM crawling_jobs WHERE id=" + id).fetchone()
    url = job[1].encode("ascii", "ignore")
    threads = job[2]
    database_connection.close();
    crawler = Crawler(int(id), int(threads), "crawling_" + id, url)

channel.basic_consume(queue=config["url_crawling_jobs_queue"], on_message_callback=start_crawling, auto_ack=True)
print(" [*] Waiting for messages. To exit press CTRL+C")
channel.start_consuming()
