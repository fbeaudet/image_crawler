FROM ubuntu:18.04
WORKDIR /image_crawler
COPY . /image_crawler

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y python python-pip python-bs4 curl rabbitmq-server
RUN update-rc.d rabbitmq-server defaults

RUN pip install flask pika BeautifulSoup requests

ENV FLASK_ENV=development
EXPOSE 8080:8080

CMD ./container_boot.sh