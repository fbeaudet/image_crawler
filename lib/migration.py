import sqlite3

def create_tables(database_connection_string):
    database_connection = sqlite3.connect(database_connection_string)
    cursor = database_connection.cursor()
    cursor.execute('CREATE TABLE IF NOT EXISTS crawling_requests (id integer primary key autoincrement)')
    cursor.execute('CREATE TABLE IF NOT EXISTS crawling_jobs (id integer primary key autoincrement, url text, threads real, results text, request_id integer)')
    database_connection.close();
