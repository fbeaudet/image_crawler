#!/bin/bash

service rabbitmq-server start &
set -e

until timeout 1 bash -c "cat < /dev/null > /dev/tcp/localhost/5672"; do
    >&2 echo "Waiting on Rabbit MQ to boot..."
    sleep 1
done

python daemon.py &
python api.py
