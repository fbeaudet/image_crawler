Image Crawler
# Dependencies
* Docker
# Build
```sh
$ chmod +x ./build_and_run.sh
$ ./build_and_run.sh
```
* Build, run, and publish API to port 80
# Request crawling
```sh
$ chmod +x ./test.sh
$ ./test.sh
```
* Request crawling on yahoo.com & example.com, return a status URL
# Example usage
./build_and_run.sh
```sh
╰─$ ./build_and_run.sh
[...]
Waiting on Rabbit MQ to boot...
   ...done.
 * Serving Flask app "api" (lazy loading)
 * Environment: development
 * Debug mode: on
 * Running on http://0.0.0.0:8080/ (Press CTRL+C to quit)
 * Restarting with stat
 * Debugger is active!
 * Debugger PIN: 236-035-556
```
./test.sh
```sh
╰─$ ./test.sh
[...]
[{"url": "https://yahoo.com", "completed": false}, {"url": "https://example.com", "completed": false}]

http://localhost/status?id=1
```
Query status URL to get results once completed