import pika
import re
import requests
from bs4 import BeautifulSoup
from threading import Thread
from config import config

class Parser:
    def __init__(self, crawler, channel_key, thread_id):
        self.crawler = crawler
        self.session = requests.Session()
        self.session.headers = {"User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.109 Safari/537.36"}
        self.thread_id = thread_id
        self.thread = Thread(target=self.parse_on_url_received_by_queue, args=(channel_key,))
        self.thread.start()
        self.is_working = False        

    def get_unique_link_urls_by_html_tag(self, url):
        try:
            req = self.session.get(url)
        except requests.exceptions.RequestException as e:
            return []
        if req.status_code != 200:
            return []

        soup = BeautifulSoup(req.content, "lxml")
        matches = map(lambda a: a["href"] if a.has_attr("href") else None, soup.select("a"))
        matches = filter(lambda url: url != None, matches)
        return list(set(matches))

    def get_unique_image_urls_by_html_tag(self, url):
        try:
            req = self.session.get(url)
        except requests.exceptions.RequestException as e:
            return []
        if req.status_code != 200:
            return []

        soup = BeautifulSoup(req.content, "lxml")
        matches = map(lambda img: img["src"] if img.has_attr("src") else None, soup.select("img"))
        matches = filter(lambda url: url != None, matches)
        return list(set(matches))

    def parse_on_url_received_by_queue(self, channel_key):
        queue_connection = pika.BlockingConnection(pika.ConnectionParameters(config["queue_connection_string"]))
        channel = queue_connection.channel()
        self.queue = channel.queue_declare(queue=channel_key)
        channel.basic_consume(queue=channel_key, on_message_callback=self.parse_link_and_image_urls, auto_ack=True)
        channel.start_consuming()

    def parse_link_and_image_urls(self, ch, method, properties, body):
        params = (map(str.strip, body.split(',')))
        url = params[0]
        depth = int(params[1])

        extracted_image_urls = self.get_unique_image_urls_by_html_tag(url)
        self.crawler.update_extracted_image_urls(extracted_image_urls)
        self.crawler.push_visited_url(url)
        
        if depth < self.crawler.max_depth - 1:
            extracted_link_urls = self.get_unique_link_urls_by_html_tag(url)
            for x in range(len(extracted_link_urls)):
                self.crawler.push_url_to_process(extracted_link_urls[x], depth + 1)
        else:
            self.crawler.save_results_if_job_is_done()
