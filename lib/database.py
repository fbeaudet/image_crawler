import sqlite3
from migration import create_tables
from config import config

create_tables(config["database_connection_string"])

def create_database_connection():
    return sqlite3.connect(config["database_connection_string"])
